import spotipy
from spotipy.oauth2 import SpotifyOAuth
import json

scope = 'playlist-modify-public'
username = 'ymc645lnit3dua0e7eee4tbf3'

token = SpotifyOAuth(client_id='4ae228c4b1f24c079d1ba08b7667b394', client_secret='a7f88f59108e452687017333f7e9db59', redirect_uri='http://127.0.0.1:8080/', scope=scope)
spotifyObject = spotipy.Spotify(auth_manager = token)

#Maak nieuwe playlist aan
playlist_name = input("Typ nieuwe playlist naam: ")
playlist_description = input("Typ nieuwe playlist beschrijving: ")

spotifyObject.user_playlist_create(user=username,name=playlist_name,public=True,description=playlist_description)

user_input = input('Geef een nummer op (of typ "stop"): ')
list_of_songs = []

#Wanneer de gebruiker geen 'stop' zegt (en dus nummers blijft toevoegen)
while user_input != 'stop':
    results = spotifyObject.search(q=user_input)
    print(results['tracks']['items'][0]['name'])                                            #Print titel van het nummer
    print("By ", results['tracks']['items'][0]['artists'][0]['name'])                       #Print artiest van het nummer
    #print("And ", results['tracks']['items'][0]['artists'][1]['name'])                      #Print tweede artiest van het nummer (Error als geen tweede artiest is om weer te geven)
    print("Nummer succesvol toegevoegd aan rij")                                                     #Bevestiging voor als dat wel gewoon goed gaat
    list_of_songs.append(results['tracks']['items'][0]['uri'])
    user_input = input('Geef een nummer (of typ "stop"): ')



#Vind de nieuw gemaakte playlist
prePlaylist = spotifyObject.user_playlists(user=username)
playlist = prePlaylist['items'][0]['id']

#Voeg nummers toe
spotifyObject.user_playlist_add_tracks(user=username, playlist_id=playlist, tracks=list_of_songs)
print("Playlist is succesvol gegenereerd!")
