command "pip install -r requirements.txt" voor het installeren van de nodige dependencies

Programmaatje die gebruik maakt van de lightweight Spotify API (Spotipy).

Dit programma maakt het mogelijk om via Python:
 Playlists aan te maken
 Deze een juiste description te geven
 Nummers aan deze playlist toe te voegen (Waarbij het gekozen nummer incl. artiest(en) teruggeprint wordt)
 Met melding als het aanmaken van de playlist gelukt is

Disclaimer:
Op dit moment werkt dit programma met een Spotify testaccount.
Om de communicatie tussen account en API werkend te hebben zijn hier ID's voor nodig.
Deze ID's werken dus ook enkel met dit testaccount.


